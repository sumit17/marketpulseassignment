package com.example.marketpulse;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.marketpulse.deps.DaggerDeps;
import com.example.marketpulse.deps.Deps;
import com.example.marketpulse.networking.NetworkModule;

import java.io.File;

public class BaseApp  extends AppCompatActivity {

    Deps deps;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        File cacheFile = new File(getCacheDir(), "responses");
        deps = DaggerDeps.builder().networkModule(new NetworkModule(cacheFile)).build();
    }

    public Deps getDeps() {
        return deps;
    }
}
