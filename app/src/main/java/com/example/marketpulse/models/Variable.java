package com.example.marketpulse.models;

import androidx.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Variable<T> {

    @SerializedName("type")
    @Expose
    private String type;

    @Nullable
    @SerializedName("values")
    @Expose
    private List<T> values = null;

    @Nullable
    @SerializedName("study_type")
    @Expose
    private String studyType;

    @Nullable
    @SerializedName("parameter_name")
    @Expose
    private String parameterName;

    @Nullable
    @SerializedName("min_value")
    @Expose
    private Integer minValue;

    @Nullable
    @SerializedName("max_value")
    @Expose
    private Integer maxValue;

    @Nullable
    @SerializedName("default_value")
    @Expose
    private Integer defaultValue;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<T> getValues() {
        return values;
    }

    public void setValues(List<T> values) {
        this.values = values;
    }

    public String getStudyType() {
        return studyType;
    }

    public void setStudyType(String studyType) {
        this.studyType = studyType;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public Integer getMinValue() {
        return minValue;
    }

    public void setMinValue(Integer minValue) {
        this.minValue = minValue;
    }

    public Integer getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(Integer maxValue) {
        this.maxValue = maxValue;
    }

    public Integer getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(Integer defaultValue) {
        this.defaultValue = defaultValue;
    }

}
