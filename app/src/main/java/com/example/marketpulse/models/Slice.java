package com.example.marketpulse.models;

import java.util.List;

public class Slice {
    int start;
    int end;
    boolean isRangePicker;
    List<Double> allowedValues;
    int minValue;
    int maxValue;

    public Slice(int start, int end, boolean isRangePicker, int minValue, int maxValue) {
        this.start = start;
        this.end = end;
        this.isRangePicker = isRangePicker;
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    public Slice(int start, int end, boolean isRangePicker, List<Double> allowedValues) {
        this.start = start;
        this.end = end;
        this.isRangePicker = isRangePicker;
        this.allowedValues = allowedValues;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public boolean isRangePicker() {
        return isRangePicker;
    }

    public void setRangePicker(boolean rangePicker) {
        isRangePicker = rangePicker;
    }

    public List<Double> getAllowedValues() {
        return allowedValues;
    }

    public void setAllowedValues(List<Double> allowedValues) {
        this.allowedValues = allowedValues;
    }

    public int getMinValue() {
        return minValue;
    }

    public void setMinValue(int minValue) {
        this.minValue = minValue;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }
}
