package com.example.marketpulse.scan;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.marketpulse.R;
import com.example.marketpulse.models.ScanData;

import java.util.List;

public class ScanAdapter extends RecyclerView.Adapter<ScanAdapter.ViewHolder> {
    private final OnItemClickListener listener;
    private List<ScanData> scanDataList;

    public ScanAdapter(List<ScanData> scanDataList, OnItemClickListener listener) {
        this.scanDataList = scanDataList;
        this.listener = listener;
    }


    @Override
    public ScanAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_scan, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ScanAdapter.ViewHolder holder, int position) {
        holder.bindView(scanDataList.get(position));
    }


    @Override
    public int getItemCount() {
        return scanDataList.size();
    }


    public interface OnItemClickListener {
        void onClick(ScanData scanData);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView header;
        TextView subHeader;
        View root;

        public ViewHolder(View itemView) {
            super(itemView);
            root = itemView;
            header = itemView.findViewById(R.id.tv_header);
            subHeader = itemView.findViewById(R.id.tv_sub_header);
        }

        public void bindView(final ScanData scanData) {
            header.setText(scanData.getName());
            subHeader.setText(scanData.getTag());
            root.setOnClickListener(view -> click(scanData));
        }

        public void click(final ScanData scanData) {
            itemView.setOnClickListener(v -> listener.onClick(scanData));
        }

    }


}
