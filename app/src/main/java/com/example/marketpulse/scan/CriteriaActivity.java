package com.example.marketpulse.scan;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;

import com.example.marketpulse.BaseApp;
import com.example.marketpulse.R;
import com.example.marketpulse.models.Criteria;
import com.example.marketpulse.models.ScanData;
import com.example.marketpulse.models.Slice;
import com.example.marketpulse.models.Variable;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import static com.example.marketpulse.util.Constants.AND;
import static com.example.marketpulse.util.Constants.INTENT_IS_RANGE_PICKER;
import static com.example.marketpulse.util.Constants.INTENT_MAX_VALUE;
import static com.example.marketpulse.util.Constants.INTENT_MIN_VALUE;
import static com.example.marketpulse.util.Constants.INTENT_SCAN_DATA;
import static com.example.marketpulse.util.Constants.INTENT_VALUES;

public class CriteriaActivity extends BaseApp {

    TextView tvCriteria;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        renderView();
        if (getIntent() != null && getIntent().hasExtra(INTENT_SCAN_DATA)) {
            init((ScanData) getIntent().getSerializableExtra(INTENT_SCAN_DATA));
        } else {
            finish();
        }
    }

    public  void renderView() {
        setContentView(R.layout.activity_criteria);
        tvCriteria = findViewById(R.id.tv_criteria);
    }

    public void init(ScanData scanData) {
        int[] index = new int[1];
        for (int i = 0; i < scanData.getCriteria().size(); i++) {
            Criteria criteria = scanData.getCriteria().get(i);
            if (criteria.getType().equals("variable")) {
                List<Slice> sliceList = new ArrayList<>();
                tvCriteria.append(getReplacedTexts(criteria, index, sliceList));
            } else {
                tvCriteria.append(criteria.getText());
            }
            if (i + 1 < scanData.getCriteria().size()) {
                tvCriteria.append(AND);
            }
        }
        makeLinksFocusable(tvCriteria);
    }

    private SpannableString getReplacedTexts(Criteria criteria, int[] index, List<Slice> sliceList) {
        LinkedHashMap keys = ((LinkedHashMap) criteria.getVariable());
        String result = criteria.getText();
        for(Object v : keys.keySet()) {
            index[0]++;
            String toReplace = "$" + index[0];
            int start = result.indexOf(toReplace);
            Variable variable = new Gson().fromJson(keys.get(v).toString(), Variable.class);
            String value;
            if (variable.getType().equals("value")) {
                value = variable.getValues().get(0).toString();
                int end = result.indexOf(toReplace) + value.length();
                sliceList.add(new Slice(start, end, false, variable.getValues()));
            } else {
                value = variable.getDefaultValue() + "";
                int end = result.indexOf(toReplace) + value.length();
                sliceList.add(new Slice(start, end, true,
                        variable.getMinValue(), variable.getMaxValue()));
            }
            result = result.replace(toReplace, value);
        }
        return createLinks(result, sliceList);
    }

    private SpannableString createLinks(String text, List<Slice> sliceList) {
        SpannableString span = new SpannableString(text);
        for (Slice slice : sliceList) {
            span.setSpan(new ClickableString(view -> {
                        onSliceClicked(slice);
                    }), slice.getStart(), slice.getEnd(),
                    SpannableString.SPAN_INCLUSIVE_EXCLUSIVE);
        }
        return span;
    }

    private void onSliceClicked(Slice slice) {
        Intent intent = new Intent(CriteriaActivity.this, PickerActivity.class);
        intent.putExtra(INTENT_IS_RANGE_PICKER, slice.isRangePicker());
        if (slice.isRangePicker()) {
            intent.putExtra(INTENT_MIN_VALUE, slice.getMinValue());
            intent.putExtra(INTENT_MAX_VALUE, slice.getMaxValue());
        } else {
            intent.putExtra(INTENT_VALUES, (ArrayList<Double>) slice.getAllowedValues());
        }
        startActivity(intent);
    }

    private void makeLinksFocusable(TextView tv) {
        MovementMethod m = tv.getMovementMethod();
        if (!(m instanceof LinkMovementMethod) && tv.getLinksClickable()) {
            tv.setMovementMethod(LinkMovementMethod.getInstance());
        }
    }

    private static class ClickableString extends ClickableSpan {

        private View.OnClickListener mListener;

        public ClickableString(View.OnClickListener listener) {
            mListener = listener;
        }

        @Override
        public void onClick(View v) {
            mListener.onClick(v);
        }
    }

}
