package com.example.marketpulse.scan;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.ViewFlipper;

import com.example.marketpulse.R;

import java.util.Collections;
import java.util.List;

import static com.example.marketpulse.util.Constants.INTENT_IS_RANGE_PICKER;
import static com.example.marketpulse.util.Constants.INTENT_MAX_VALUE;
import static com.example.marketpulse.util.Constants.INTENT_MIN_VALUE;
import static com.example.marketpulse.util.Constants.INTENT_VALUES;

public class PickerActivity extends AppCompatActivity {

    ViewFlipper viewFlipper;
    ListView listView;
    NumberPicker numberPicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picker);
        renderView();
        init();
    }

    public  void renderView() {
        setContentView(R.layout.activity_picker);
        viewFlipper = findViewById(R.id.viewFlipper);
        listView = findViewById(R.id.listView);
        numberPicker = findViewById(R.id.numberPicker);
    }

    public void init() {
        boolean isRangePicker = getIntent().getBooleanExtra(INTENT_IS_RANGE_PICKER, false);
        if (isRangePicker) {
            viewFlipper.setDisplayedChild(1);
            numberPicker.setMinValue(getIntent().getIntExtra(INTENT_MIN_VALUE, 0));
            numberPicker.setMaxValue(getIntent().getIntExtra(INTENT_MAX_VALUE, 100));
        } else {
            List<Double> values = (List<Double>) getIntent().getSerializableExtra(INTENT_VALUES);
            Collections.sort(values);
            ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, values);
            listView.setAdapter(adapter);
        }
    }

}
