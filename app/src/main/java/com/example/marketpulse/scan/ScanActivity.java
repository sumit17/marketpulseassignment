package com.example.marketpulse.scan;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marketpulse.BaseApp;
import com.example.marketpulse.R;
import com.example.marketpulse.models.ScanData;
import com.example.marketpulse.networking.Service;

import java.util.List;

import javax.inject.Inject;

import static com.example.marketpulse.util.Constants.INTENT_SCAN_DATA;

public class ScanActivity extends BaseApp implements ScanView {

    private RecyclerView list;
    @Inject
    public Service service;
    ProgressBar progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDeps().inject(this);
        renderView();
        init();
        ScanPresenter presenter = new ScanPresenter(service, this);
        presenter.getScanData();
    }

    public  void renderView() {
        setContentView(R.layout.activity_scan);
        list = findViewById(R.id.list);
        progressBar = findViewById(R.id.progress);
    }

    public void init(){
        list.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void showWait() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void removeWait() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onFailure(String appErrorMessage) {

    }

    @Override
    public void getScanDataSuccess(List<ScanData> scanDataList) {
        ScanAdapter adapter = new ScanAdapter(scanDataList, scanData -> {
            Intent intent = new Intent(ScanActivity.this, CriteriaActivity.class);
            intent.putExtra(INTENT_SCAN_DATA, scanData);
            startActivity(intent);
        });
        list.setAdapter(adapter);
    }
}
