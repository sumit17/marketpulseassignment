package com.example.marketpulse.scan;

import com.example.marketpulse.models.ScanData;
import com.example.marketpulse.networking.NetworkError;
import com.example.marketpulse.networking.Service;

import java.util.List;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class ScanPresenter {
    private final Service service;
    private final ScanView view;
    private CompositeSubscription subscriptions;

    public ScanPresenter(Service service, ScanView view) {
        this.service = service;
        this.view = view;
        this.subscriptions = new CompositeSubscription();
    }

    public void getScanData() {
        view.showWait();

        Subscription subscription = service.getScanData(new Service.GetScanDataCallback() {
            @Override
            public void onSuccess(List<ScanData> scanDataList) {
                view.removeWait();
                view.getScanDataSuccess(scanDataList);
            }

            @Override
            public void onError(NetworkError networkError) {
                view.removeWait();
                view.onFailure(networkError.getAppErrorMessage());
            }

        });

        subscriptions.add(subscription);
    }
    public void onStop() {
        subscriptions.unsubscribe();
    }
}
