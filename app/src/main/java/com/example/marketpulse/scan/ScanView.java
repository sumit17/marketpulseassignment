package com.example.marketpulse.scan;

import com.example.marketpulse.models.ScanData;

import java.util.List;

public interface ScanView {
    void showWait();

    void removeWait();

    void onFailure(String appErrorMessage);

    void getScanDataSuccess(List<ScanData> scanDataList);

}
