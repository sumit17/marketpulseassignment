package com.example.marketpulse.deps;

import com.example.marketpulse.scan.ScanActivity;
import com.example.marketpulse.networking.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NetworkModule.class,})
public interface Deps {
    void inject(ScanActivity scanActivity);
}
