package com.example.marketpulse.networking;

import com.example.marketpulse.models.ScanData;

import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class Service {
    private final NetworkService networkService;

    public Service(NetworkService networkService) {
        this.networkService = networkService;
    }

    public Subscription getScanData(final GetScanDataCallback callback) {

        return networkService.getScanDataList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(Observable::error)
                .subscribe(new Subscriber<List<ScanData>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(List<ScanData> scanDataList) {
                        callback.onSuccess(scanDataList);

                    }
                });
    }

    public interface GetScanDataCallback {
        void onSuccess(List<ScanData> scanDataList);
        void onError(NetworkError networkError);
    }
}
