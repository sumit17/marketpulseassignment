package com.example.marketpulse.networking;

import com.example.marketpulse.models.ScanData;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

public interface NetworkService {

    @GET("data")
    Observable<List<ScanData>> getScanDataList();

}
