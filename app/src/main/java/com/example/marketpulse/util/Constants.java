package com.example.marketpulse.util;

public class Constants {

    public static final String INTENT_SCAN_DATA = "INTENT_SCAN_DATA";
    public static final String INTENT_IS_RANGE_PICKER = "INTENT_IS_RANGE_PICKER";
    public static final String INTENT_MIN_VALUE = "INTENT_MIN_VALUE";
    public static final String INTENT_MAX_VALUE = "INTENT_MAX_VALUE";
    public static final String INTENT_VALUES = "INTENT_VALUES";
    public static final String AND = "\nand\n";

}
