package com;

import com.example.marketpulse.models.ScanData;
import com.example.marketpulse.networking.NetworkService;
import com.example.marketpulse.networking.Service;
import com.example.marketpulse.scan.ScanPresenter;
import com.example.marketpulse.scan.ScanView;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import rx.Observable;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ScanPresenterTest {

    @Rule
    public final RxSchedulersOverrideRule mOverrideSchedulersRule = new RxSchedulersOverrideRule();

    @Mock
    private NetworkService networkService;

    @Mock
    private ScanView scanView;

    @Mock
    private List<ScanData> scanDataList;

    private Service service;
    private ScanPresenter scanPresenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        service = new Service(networkService);
        scanPresenter = new ScanPresenter(service, scanView);
    }

    @After
    public void teardown() {
        scanPresenter.onStop();
    }

    @Test
    public void loadScanDataFromAPIAndLoadIntoView() {

        Observable<List<ScanData>> responseObservable = Observable.just(scanDataList);
        when(networkService.getScanDataList()).thenReturn(responseObservable);

        scanPresenter.getScanData();

        InOrder inOrder = Mockito.inOrder(scanView);
        inOrder.verify(scanView).showWait();
        inOrder.verify(scanView).removeWait();
        inOrder.verify(scanView).getScanDataSuccess(scanDataList);
    }
}
